import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fb-catalog-promo',
  template: `
    <h1>Promo</h1>
    <button routerLink="../">Back to home</button>
  `,
  styles: [
  ]
})
export class CatalogPromoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
