import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'fb-devices-os-icon',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <i
      class="fa"
      [ngClass]="{
          'fa-android': os === 'android',
          'fa-apple': os === 'ios'
        }"
      [style.color]="os === 'ios' ? 'grey' : 'lightgreen'"
    ></i>
  `,
})
export class DevicesOsIconComponent {
  @Input() os: 'android' | 'ios';

}
