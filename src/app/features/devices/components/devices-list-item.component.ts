import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Device } from '../../../model/device';

@Component({
  selector: 'fb-devices-list-item',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `

    <li
      class="list-group-item"
      (click)="opened = !opened"
    >
      <fb-devices-os-icon [os]="device.os"></fb-devices-os-icon>
     
      {{device.id}} - {{device.label}}

      <i class="fa fa-trash pull-right"
         (click)="deleteHandler(device, $event)"></i>
      <div class="pull-right">{{device.cost}}</div>
      <div *ngIf="opened">bla bla</div>

    </li>
    
  `,
})
export class DevicesListItemComponent {
  @Input() device: Device;
  @Output() delete: EventEmitter<Device> = new EventEmitter<Device>()
  opened: boolean;

  deleteHandler(device: Device, event: MouseEvent): void {
    event.stopPropagation();
    this.delete.emit(device);
  }

}
