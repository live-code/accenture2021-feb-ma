import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'fb-card',
  template: `
    <div 
      class="card" 
      [ngClass]="{'mb-3': bottomMargin}"
    >
      <!--header-->
      <div 
        class="card-header" 
        [ngClass]="{
          'bg-dark text-white': headerCls === 'dark',
          'bg-success': headerCls === 'success'
        }"
        (click)="toggle.emit()"
      >
        {{title}}
        <i
          *ngIf="icon"
          (click)="iconClickHandler($event)"
          class="pull-right" [ngClass]="icon"></i>
      </div>
      
      <!--body-->
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
      
      <!--footer-->
      <div *ngIf="opened">
        <div 
          class="card-footer"
          *ngIf="footerButtonLabel || footerButtonLabel"
        >
          {{footerLabel}}
          <button
            *ngIf="footerButtonLabel"
            (click)="footerButtonClick.emit()"
            class="pull-right btn btn-outline-primary">
            {{footerButtonLabel}}
          </button>
        </div>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title = 'CARD';
  @Input() bottomMargin = true;
  @Input() icon: string;
  @Input() headerCls: 'success' | 'dark';
  @Input() footerLabel: string;
  @Input() footerButtonLabel: string;
  @Input() body: string;
  @Input() opened = true;

  @Output() iconClick: EventEmitter<void> = new EventEmitter();
  @Output() footerButtonClick: EventEmitter<void> = new EventEmitter();
  @Output() toggle: EventEmitter<void> = new EventEmitter();

  iconClickHandler(event: MouseEvent): void {
    event.stopPropagation();
    this.iconClick.emit()
  }
}
