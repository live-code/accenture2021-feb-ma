import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDirectivesRoutingModule } from './uikit-directives-routing.module';
import { UikitDirectivesComponent } from './uikit-directives.component';
import { SharedModule } from '../../shared/shared.module';
import { IfLoggedModule } from '../../core/auth/if-logged.module';


@NgModule({
  declarations: [UikitDirectivesComponent],
  imports: [
    CommonModule,
    UikitDirectivesRoutingModule,
    SharedModule,
    IfLoggedModule
  ]
})
export class UikitDirectivesModule { }
