 import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fb-uikit-directives',
  template: `

 
    <h1>iflogged</h1>
   
    <button *fbIfSignin> Button if logged structural</button>
    
    <button fbIfLogged> Button if logged</button> 
    <button fbIfRole="admin">Butto for admins</button> 
    <button fbIfRole="moderator">Butto for moderators</button> 
    
    <hr>
    <h1>Margin</h1>
    <div [fbMargin] fbUrl="http://www.google.com">Google</div>
    <button [fbMargin]="4" fbUrl="http://www.microsoft.com">Microsoft</button>
    <hr>

    <h1>Bg directive</h1>
    <div fbBg="success" style="padding: 30px">xxx</div>
    <hr>
    <h1>Pad </h1>
    <div [fbBg]="'danger'" [fbPad]></div>
    <div [fbBg]="'danger'" [fbPad]="value"></div>
    <button (click)="value = value + 1">+</button>
    
  `,
})
export class UikitDirectivesComponent{
  value = 1;


}
