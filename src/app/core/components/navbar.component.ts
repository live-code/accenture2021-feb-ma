import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../auth/authentication.service';

@Component({
  selector: 'fb-navbar',
  template: `
    <a routerLink="catalog" routerLinkActive="activeMenu">catalog</a> -
    <a fbIfLogged
      routerLink="devices" routerLinkActive="activeMenu">devices</a> -
    <a routerLink="uikit" routerLinkActive="activeMenu">uikit</a> - 
    <a routerLink="uikit-directive" routerLinkActive="activeMenu">uikit-directive</a> - 
    <a routerLink="login" routerLinkActive="activeMenu">login</a>
    - <span *fbIfSignin (click)="logoutHandler()">Logout</span>
    
    {{authenticationService.data?.displayName}}
  `,
  styles: [`
    .activeMenu {
      background-color: orange;
    }
  `]
})
export class NavbarComponent {

  constructor(public authenticationService: AuthenticationService) { }


  logoutHandler(): void {
    this.authenticationService.logout()
  }
}
