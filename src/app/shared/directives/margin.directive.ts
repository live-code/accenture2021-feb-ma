import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[fbMargin]'
})
export class MarginDirective {
  @Input() set fbMargin(val: number) {
    this.setMargin(val);
  }

  setMargin(val): void {
    const el = this.el.nativeElement;
    const multiplier = val || 1;
    el.style.margin = (multiplier * 10) + 'px';
  }

  constructor(private el: ElementRef) {
    this.setMargin(1);
  }

}
