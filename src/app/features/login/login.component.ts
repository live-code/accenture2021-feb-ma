import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../../core/auth/authentication.service';

@Component({
  selector: 'fb-login',
  template: `
    
    <form [formGroup]="form" (submit)="loginHandler()">
      <br>
      <input 
        formControlName="username" type="text" placeholder="username"
        [style.background]="form.get('username').valid ? 'green' : 'red'"
      >

      <br>
      <input 
        formControlName="password" type="text" placeholder="password"
        [style.background]="form.get('password').valid ? 'green' : 'red'">
      <button [disabled]="form.invalid">Submit</button>
    </form>
  `
})
export class LoginComponent {
  form: FormGroup;

  constructor(private fb: FormBuilder, private authService: AuthenticationService) {
    this.form = fb.group({
      username: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      password: ['', Validators.compose([Validators.required, alphaNumeric])],
    });
  }

  loginHandler(): void {
    this.authService.login(
      this.form.value.username,
      this.form.value.password,
    );
  }
}

const ALPHA_NUMERIC_REGEX = /^([A-Za-z]|[0-9]|_)+$/;

export function alphaNumeric(c: FormControl): ValidationErrors {
  if (c.value && !c.value.match(ALPHA_NUMERIC_REGEX)) {
    return {
      alphanumeric: true
    };
  }
  return null;
}
