import { Directive, HostBinding, Input } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { UserRoles } from '../../model/user-roles';

@Directive({
  selector: '[fbIfRole]'
})
export class IfRoleDirective {
  @Input() fbIfRole: UserRoles;

  @HostBinding('style.display') get display(): string {
    return this.authService.data?.role === this.fbIfRole ?
      null : 'none';
  }

  constructor(private authService: AuthenticationService) {
  }
}
