import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from './auth';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  data: Auth;
  data$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(null);
  isLogged$ = this.data$.pipe(map(data => !!data));
  role$ = this.data$.pipe(map(data => data.role));

  constructor(
    private http: HttpClient,
    private router: Router
  ) {
    this.data = JSON.parse(localStorage.getItem('auth')) as Auth;
  }

  login(username: string, password: string): void {
    const params = new HttpParams()
      .set('username', username)
      .set('password', password);

    this.http.get<Auth>('http://localhost:3000/login', { params })
      .subscribe(res => {
        this.data = res;
        this.data$.next(res);
        this.router.navigateByUrl('devices');
        localStorage.setItem('auth', JSON.stringify(res));
      });
  }

  logout(): void {
    this.data = null;
    this.data$.next(null);
    this.router.navigateByUrl('login');
    localStorage.removeItem('auth')
  }

  isLogged(): boolean {
    return !!this.data;
  }

}
