import { Component, ViewEncapsulation } from '@angular/core';
import { Country } from '../../model/country';

@Component({
  selector: 'fb-uikit',
  template: `
    
    <div class="container mt-3">
      <fb-tabbar 
        [items]="countries" 
        [active]="activeCountry"
        (tabClick)="selectCountry($event)"></fb-tabbar>
      
      <div>{{activeCountry?.description}}</div>
      
      <img
        *ngIf="activeCountry"
        [src]="'https://maps.googleapis.com/maps/api/staticmap?center=' + activeCountry?.label + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k'" alt="">
      
      <br>
      <br>
      <br>
      <br>
      <button>Open Card</button>

      <fb-card 
        [title]="'Accenture'"
        [opened]="card1IsOpened"
        [headerCls]="'success'"
        icon="fa fa-link"
        footerLabel="copyright 2021"
        footerButtonLabel="VISIT WEB SITE"
        (iconClick)="card2IsOpened = !card2IsOpened"
        (footerButtonClick)="openLink('http://www.google.com')"
        (toggle)="card1IsOpened = !card1IsOpened"
      >
        <form>
          <input type="text">
          <input type="text">
          <input type="text">
        </form>
        
      </fb-card>
      
      <fb-card
        [opened]="card2IsOpened"
        headerCls="dark"
        icon="fa fa-list"
        (iconClick)="openLink('http://www.google.com')"
        [title]="'Panel 2'"
      >
        <h1>ciao ciao</h1>
      </fb-card>

      
      <fb-card title="card 3">
        <div class="row">
          <div class="col">
            <fb-card title="left"></fb-card>
          </div>
          <div class="col">
            <fb-card title="right"></fb-card>
          </div>
        </div>
      </fb-card>
    </div>
    
  `,
})
export class UikitComponent {
  countries: Country[];
  activeCountry: Country;
  card1IsOpened = false;
  card2IsOpened = false;

  constructor() {
    setTimeout(() => {
      this.countries = [
        { id: 1, label: 'Italy', description: 'bla bla 1'},
        { id: 2, label: 'Germany', description: 'bla bla 2'},
        { id: 3, label: 'Spain', description: 'bla bla 3'},
      ];
      this.activeCountry = this.countries[0];
    }, 2000);
  }
  openLink(url: string): void {
    window.open(url);
  }
  openAlert(): void {
    window.alert('...');
  }

  selectCountry(item: Country): void {
    console.log('select country', item.description)
    this.activeCountry = item;
  }
}
