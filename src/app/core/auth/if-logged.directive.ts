import { Directive, HostBinding } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Directive({
  selector: '[fbIfLogged]'
})
export class IfLoggedDirective {
  @HostBinding('style.display') get display(): string {
    return this.authService.isLogged() ? null : 'none';
  }

  constructor(private authService: AuthenticationService) {
  }
}
