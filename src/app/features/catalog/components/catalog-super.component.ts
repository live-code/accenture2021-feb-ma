import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fb-catalog-super',
  template: `
    <h1>Super</h1>
  `,
  styles: [
  ]
})
export class CatalogSuperComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
