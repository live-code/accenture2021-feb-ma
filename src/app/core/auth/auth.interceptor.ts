import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let cloneReq = req;
    if (this.authenticationService.isLogged()) {
       cloneReq = req.clone({
        setHeaders: {
          Authentication: this.authenticationService.data.token,
        }
      });
    }
    return next.handle(cloneReq)
      .pipe(
        // keep it alive obs
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              case 404:
                break;

              default:
              case 0:
              case 401:
                // this.authenticationService.logout()
                alert('errore token scaduto')
                break;
            }
          }
          return throwError(err)
          // return of(null)
        })
      );
  }
}


// of(1, 2, 3).subscribe(val => )
