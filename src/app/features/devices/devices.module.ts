import { NgModule } from '@angular/core';
import { DevicesComponent } from './devices.component';
import { DevicesFormComponent } from './components/devices-form.component';
import { DevicesListComponent } from './components/devices-list.component';
import { DevicesListItemComponent } from './components/devices-list-item.component';
import { DevicesOsIconComponent } from './components/devices-os-icon.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    DevicesComponent,
      DevicesFormComponent,
      DevicesListComponent,
        DevicesListItemComponent,
          DevicesOsIconComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      { path: '', component: DevicesComponent}
    ])
  ]
})
export class DevicesModule {

}

