import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IfLoggedDirective } from './if-logged.directive';
import { IfRoleDirective } from './if-role.directive';
import { IfSigninDirective } from './if-signin.directive';

@NgModule({
  declarations: [IfLoggedDirective, IfRoleDirective, IfSigninDirective],
  exports: [IfLoggedDirective, IfRoleDirective, IfSigninDirective],
  imports: [
    CommonModule
  ]
})
export class IfLoggedModule { }
