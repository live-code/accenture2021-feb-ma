import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fb-catalog',
  template: `
    <button routerLink="home" routerLinkActive="activeMenu">home</button>
    <button routerLink="promo" routerLinkActive="activeMenu">promo</button>
    <button routerLink="super" routerLinkActive="activeMenu">super</button>

    <router-outlet></router-outlet>
  `,
  styles: [`
    .activeMenu {
      background-color: green;
    }
 ` ]
})
export class CatalogComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
