import { ChangeDetectionStrategy, Component } from '@angular/core';
import { DevicesService } from './services/devices.service';

@Component({
  selector: 'fb-devices',
  template: `

    <div *ngIf="deviceService.error">Errore server</div>
    <fb-card title="FORM">
      <fb-devices-form 
        (add)="deviceService.addHandler($event)"></fb-devices-form>
    </fb-card>
    
    <fb-card title="LIST">
      <fb-devices-list 
        [items]="deviceService.devices" 
        (delete)="deviceService.deleteHandler($event)"></fb-devices-list>
    </fb-card>
    
    <pre>{{deviceService.devices | json}}</pre>
  `,
})
export class DevicesComponent {
  constructor(public deviceService: DevicesService) {
    deviceService.getAll();
  }
}
