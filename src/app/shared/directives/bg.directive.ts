import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[fbBg]'
})
export class BgDirective {
  @Input() fbBg: 'success' | 'danger';
  @HostBinding() innerText = 'ciao ACN';
  @HostBinding() get className(): string {
    return 'alert alert-' + this.fbBg;
  }

}
