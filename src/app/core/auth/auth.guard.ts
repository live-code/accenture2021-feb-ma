import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { delay, map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private http: HttpClient
  ) {}

  // sync guard
  canActivate(): boolean {
    if (!this.authService.isLogged()) {
      this.router.navigateByUrl('');
    }
    return this.authService.isLogged();
  }

  canActivateWithObs(): Observable<boolean> {
    return this.authService.isLogged$;
  }

  // async guard
  canActivate2(): Observable<boolean> {
    return this.http.get<{ result: string }>('http://localhost:3000/validateUser')
      .pipe(
        delay(1000),
        map(res => res.result === 'ok'),
        tap(isLogged => {
          if (!isLogged) {
            this.router.navigateByUrl('');
          }
        })
      );
  }
}
