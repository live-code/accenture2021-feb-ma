import { Directive, HostBinding, Input } from '@angular/core';

const STEP_SIZE = 30;

@Directive({
  selector: '[fbPad]'
})
export class PadDirective {
  @Input() fbPad = 1;

  @HostBinding('style.padding') get padding(): string {
    return (STEP_SIZE * this.fbPad) + 'px';
  }

}
