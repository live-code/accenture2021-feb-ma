import { NgModule } from '@angular/core';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';
import { CommonModule } from '@angular/common';
import { BgDirective } from './directives/bg.directive';
import { PadDirective } from './directives/pad.directive';
import { MarginDirective } from './directives/margin.directive';
import { UrlDirective } from './directives/url.directive';

@NgModule({
  declarations: [
    CardComponent,
    TabbarComponent,
    BgDirective,
    PadDirective,
    MarginDirective,
    UrlDirective,
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    CardComponent,
    TabbarComponent,
    BgDirective,
    PadDirective,
    MarginDirective,
    UrlDirective
  ]
})
export class SharedModule {

}
