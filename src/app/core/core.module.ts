import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './components/navbar.component';
import { RouterModule } from '@angular/router';
import { IfLoggedDirective } from './auth/if-logged.directive';
import { IfLoggedModule } from './auth/if-logged.module';

@NgModule({
  declarations: [NavbarComponent],
  exports: [NavbarComponent],
  imports: [
    CommonModule,
    RouterModule,
    IfLoggedModule
  ]
})
export class CoreModule { }
