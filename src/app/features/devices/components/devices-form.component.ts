import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Device } from '../../../model/device';

@Component({
  selector: 'fb-devices-form',
  template: `
    <form #f="ngForm" (submit)="add.emit(f.value)">
      <input type="text" ngModel name="label" required minlength="2">
      <input type="text" ngModel name="cost">
      <button [disabled]="f.invalid" >ADD USER</button>
    </form>
  `,
})
export class DevicesFormComponent {
  @Output() add: EventEmitter<Device> = new EventEmitter<Device>();
}
