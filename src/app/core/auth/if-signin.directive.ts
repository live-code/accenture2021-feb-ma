import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthenticationService } from './authentication.service';

@Directive({
  selector: '[fbIfSignin]'
})
export class IfSigninDirective {


  constructor(
    private template: TemplateRef<any>,
    private view: ViewContainerRef,
    private authService: AuthenticationService
  ) {
    console.log('istannce')
    // ....
    this.authService.isLogged$
      .subscribe(val => {
        if (val) {
          view.createEmbeddedView(template)
        } else {
          view.clear()
        }
      });

  }

}
