import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { CatalogPromoComponent } from './components/catalog-promo.component';
import { CatalogSuperComponent } from './components/catalog-super.component';
import { CatalogHomeComponent } from './components/catalog-home.component';


@NgModule({
  declarations: [CatalogComponent, CatalogPromoComponent, CatalogSuperComponent, CatalogHomeComponent],
  imports: [
    CommonModule,
    CatalogRoutingModule,
  ]
})
export class CatalogModule { }
