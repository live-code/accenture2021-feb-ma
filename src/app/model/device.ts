export interface Device {
  id: number;
  label: string;
  memory: number;
  cost: number;
  os: 'ios' | 'android';
}
