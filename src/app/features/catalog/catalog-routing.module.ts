import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogComponent } from './catalog.component';
import { CatalogPromoComponent } from './components/catalog-promo.component';
import { CatalogSuperComponent } from './components/catalog-super.component';
import { CatalogHomeComponent } from './components/catalog-home.component';

const routes: Routes = [
  {
    path: '',
    component: CatalogComponent,
    children: [
      { path: 'promo', component: CatalogPromoComponent },
      { path: 'super', component: CatalogSuperComponent },
      { path: 'home', component: CatalogHomeComponent },
      { path: '', redirectTo: 'home'}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogRoutingModule { }
