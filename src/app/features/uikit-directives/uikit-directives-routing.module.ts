import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitDirectivesComponent } from './uikit-directives.component';

const routes: Routes = [{ path: '', component: UikitDirectivesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitDirectivesRoutingModule { }
