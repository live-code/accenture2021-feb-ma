import { Injectable } from '@angular/core';
import { Device } from '../../../model/device';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of, throwError } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class DevicesService {
  devices: Device[];
  error: boolean;

  constructor(private http: HttpClient) {}

  getAll(): void {
    this.error = false;
    this.http.get<Device[]>('http://localhost:3000/devices')
      .subscribe(
        result => {
          console.log('result', result)
          this.devices = result;
        },
        err => {
          console.log('DEVICE GET ERROR')
          this.devices = [];
          this.error = err;
        }
      );
  }

  addHandler(device: Device): void {
    this.http.post<Device>('http://localhost:3000/devices', device)
      .subscribe(res => {
        this.devices = [  ...this.devices, res ];
      });

  }

  deleteHandler(device: Device): void {
    this.http.delete('http://localhost:3000/devices/' + device.id)
      .subscribe(() => {
        this.devices = this.devices.filter(d => d.id !== device.id);
      });
  }
}
