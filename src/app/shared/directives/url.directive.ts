import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[fbUrl]'
})
export class UrlDirective {
  @Input() fbUrl: string;

  @HostListener('click')
  function(): void {
    window.open(this.fbUrl)
  }

}
