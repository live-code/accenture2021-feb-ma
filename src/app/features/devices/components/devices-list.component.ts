import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Device } from '../../../model/device';

@Component({
  selector: 'fb-devices-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <fb-devices-list-item
      *ngFor="let device of items"
      [device]="device" 
      (delete)="delete.emit($event)"
    ></fb-devices-list-item>
  `,
})
export class DevicesListComponent  {
  @Input() items: Device[];
  @Output() delete: EventEmitter<Device> = new EventEmitter<Device>()
}
