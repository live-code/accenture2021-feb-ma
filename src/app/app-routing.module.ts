import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';

const routes = [
  {
    path: 'devices',
    canActivate: [AuthGuard],
    loadChildren: () => import('./features/devices/devices.module')
      .then(m => m.DevicesModule)
  },
  {
    path: 'uikit',
    loadChildren: () => import('./features/uikit/uikit.module')
      .then(m => m.UikitModule)
  },
  { path: 'login', data: { label: 'xyz'}, loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: 'uikit-directive', loadChildren: () => import('./features/uikit-directives/uikit-directives.module').then(m => m.UikitDirectivesModule) },
  { path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
